﻿# Urshop小程序商城
![UrShop](https://www.urshop.cn/img/cases/UrShop.png)
#### <h2>介绍</h2>
UrShop小程序商城 基于 微信小程序 + NetCore + layui 技术构建 ，多店铺商城系统。项目包含 微信小程序，管理后台，插件管理，WebApi。基于C#后台语言，达到商用标准的一套项目体系

|![小程序案例](https://www.urshop.cn/img/cases/gh_d26048f2fdd6_344.jpg)  |   ![小程序案例](https://www.urshop.cn/img/cases/gh_ffc5ea0d4167_258.jpg) |
|----|----|
|![小程序案例](https://www.urshop.cn/img/cases/gh_ddf810c52fe4_258.jpg)    | ![小程序案例](https://www.urshop.cn/img/cases/gh_97e30d686da3_258.jpg)  |




- 后台在线预览网址 https://demo.urshop.cn/admin/login  后台账号urshop 密码 admin
- API在线接口文档   http://demo.urshop.cn/apihelp/index.html
- UrShop教程文档   https://www.urshop.cn/docs/89/97.html
- Gitee仓库地址    https://gitee.com/urselect/urshop

 **> 开源不易，您的支持是我们的动力，点右上角 “Star” 进行支持🙏🙏！** 


#### <h2>技术架构</h2>
 **开发环境** 
- 语言: C#
- IDE:Visual studio 2019
- 前端：原生微信小程序
- 数据库：MySQL5.7


NET技术说明：
| 架包 | 名称 | 版本 |
|----|----|----|
| AspNet MVC  | MVC框架   |![Microsoft.AspNetCore.Mvc.Core](https://img.shields.io/nuget/v/Microsoft.AspNetCore.Mvc.Core) ![Microsoft.AspNetCore.Mvc.Core](https://img.shields.io/nuget/dt/Microsoft.AspNetCore.Mvc.Core)   |
| WebApi   |  接口框架  |![Microsoft.AspNet.WebApi.Core](https://img.shields.io/nuget/v/Microsoft.AspNet.WebApi.Core)  ![Microsoft.AspNet.WebApi.Core](https://img.shields.io/nuget/dt/Microsoft.AspNet.WebApi.Core)  |
| EntityFramework   | ORM工具   |![EntityFramework](https://img.shields.io/nuget/v/EntityFramework) ![EntityFramework](https://img.shields.io/nuget/dt/EntityFramework)   |
| Autofac |  IOC框架  |![Autofac](https://img.shields.io/nuget/v/Autofac)   ![Autofac](https://img.shields.io/nuget/dt/Autofac)    |
| AutoMapper   |  DTO映射  | ![AutoMapper](https://img.shields.io/nuget/v/AutoMapper)  ![AutoMapper](https://img.shields.io/nuget/dt/AutoMapper)   |
| FluentValidation   |  验证组件  | ![FluentValidation](https://img.shields.io/nuget/v/FluentValidation)![FluentValidation](https://img.shields.io/nuget/dt/FluentValidation)   |
| SwaggerUI   |  接口文档  | ![SwaggerUI](https://img.shields.io/nuget/v/Swashbuckle.AspNetCore) ![SwaggerUI](https://img.shields.io/nuget/dt/Swashbuckle.AspNetCore)  |
| QRCoder   | 二维码生成 |![QRCoder](https://img.shields.io/nuget/v/QRCoder) ![QRCoder](https://img.shields.io/nuget/dt/QRCoder)   |
| Newtonsoft.Json | Json工具   | ![Newtonsoft.Json](https://img.shields.io/nuget/v/Newtonsoft.Json) ![Newtonsoft.Json](https://img.shields.io/nuget/dt/Newtonsoft.Json)  |
| Redis| 高并发缓存   |  ![StackExchange.Redis](https://img.shields.io/nuget/v/StackExchange.Redis)![StackExchange.Redis](https://img.shields.io/nuget/dt/StackExchange.Redis)  |
| EPPlus | Excel工具   | ![EPPlus](https://img.shields.io/nuget/v/EPPlus) ![EPPlus](https://img.shields.io/nuget/dt/EPPlus)  |
| JwtBearer| 授权认证   | ![IdentityModel](https://img.shields.io/nuget/v/IdentityModel) ![IdentityModel](https://img.shields.io/nuget/dt/IdentityModel)  |
| WeixinSDK | 微信SDK |  ![weixinSDK](https://img.shields.io/github/forks/night-king/weixinSDK?style=social) |


#### <h2>开发计划</h2>
<ul>
<li>1 多店铺功能[测试中]</li>
<li>2 三级分销+加盟代理</li>
<li>3 官网完善每个技术点的教程与使用方法【完善中】</li>
<li>4 跟随最新的Net版本进行更新</li>
<li>5 增加更多的插件用于完善插件中心，如接入网店管家等ERP插件</li>
<li>6 开发并开源IOS、Android端</li>
<li>7 开发电脑端[开发中]</li>
</ul>

#### <h2>项目结构</h2>

```
UrShop
|--Urs.Shop 
|--Urs.Shop-Area-Admin 后台管理
|--Urs.Services 接口服务
|--Urs.Data 领域实体与映射
|--Urs.Data-Domain 领域实体
|--Urs.Data-Mapping 领域映射
|--Urs.Core 核心接口与封装
|--Urs.Framework 辅助框架
|--Plugins 插件中心
|--Plugins-Plugin.Api 商城接口插件
|--Plugins-Plugin.ExternalAuth.WeixinOpen 微信授权插件
|--Plugins-Plugin.Payments.WeixinOpen 微信支持插件
|--Plugins-Plugin.Shipping.ByWeight 按重量计费插件
|--miniprogram 微信小程序原生商城
```


#### <h2>项目截图</h2>
| ![首页](https://www.urshop.cn/img/cases/20220322165422.png)  | ![类目](https://www.urshop.cn/img/cases/20220322170118.png)  |
|---|---|
| ![商品列表](https://www.urshop.cn/img/cases/20220322170423.png) | ![商品详情](https://www.urshop.cn/img/cases/20220322170515.png)  |
| ![用户列表](https://www.urshop.cn/img/cases/20220221011037.png)  | ![用户详情](https://www.urshop.cn/img/cases/20220322171608.png)  |
| ![订单列表](https://www.urshop.cn/img/cases/20220322170717.png)  | ![订单详情](https://www.urshop.cn/img/cases/20220322171409.png)  |

| ![小程序首页](https://www.urshop.cn/img/cases/20220323001014.png)  | ![小程序分类页](https://www.urshop.cn/img/cases/20220323001011.png)  | ![小程序详情页](https://www.urshop.cn/img/cases/20220323000950.png)  |
|---|---|---|
|![小程序首页](https://www.urshop.cn/img/cases/20220322172156.png)|![小程序首页](https://www.urshop.cn/img/cases/20220322172417.png)|![购物车页](https://www.urshop.cn/img/cases/20220322172255.png)|
|![小程序首页](https://www.urshop.cn/img/cases/20220408104330.jpg)|![小程序首页](https://www.urshop.cn/img/cases/20220408104336.jpg)|![购物车页](https://www.urshop.cn/img/cases/20220408104339.jpg)|
|![分销页面](https://www.urshop.cn/img/cases/20220427155743.png)|![分销页面](https://www.urshop.cn/img/cases/20220427155752.png)|![分销页面](https://www.urshop.cn/img/cases/20220427155757.png)|





#### 功能模块
1.  支持多种电子商务交易模式，企业与个人间交易模式（B2C，网上零售）

2. SKU，支持单个产品的多产品规格（多SKU），每个SKU关联库存

3. 每个产品均提供了重量、长宽高设置，方便物流计费

4. 产品所属分类支持多个分类

5. 订单管理支持对订单状态、订单价格、支付状态进行修改

6. 订单可修改商品，客服人员可以给指定订单加赠品

7. 订单配送 商品太多,可以分拆开配送，商品支持多张配送单

8. 订单操作有记录，方便显示订单处理跟踪

9. 配送发货收货，允许后台操作人员操作修改，对于货到付款商品、用户没有及时操作确定交货的配送单。可以按情况确定交货

10. 配送提供区域限制、配送方式选择

11. 畅销表报 可以指定条件查看畅销产品

12. 低库存报表,分别你了解商品库存情况

13. 客户信息，查看用户购物车、地址、活动记录。为客户添加积分

14. 操作人员不限制，可以添加操作人员来管理指定后台板块

15. 首页幻灯片大图，主题自带幻灯片功能（功能简单，设置简单）；

16. 自带多种小插件（持续增加中）；

17. 文章页面相关文章/产品展示；

18. 热门产品轮播展示模块；

#### <h2>使用说明</h2>

1.  在线预览网址 https://demo.urshop.cn  后台账号urshop 密码 admin
2.  urshpodb.sql为数据库文件（目前只提供了mysql 5.7.33版本）
3.  Urs.Shop/App_Data/dataSettings.json 数据库配置DataConnectionString server=localhost;port=3306;database=urshopdb;user id=root;password=123456;persistsecurityinfo=False;allowuservariables=True;defaultcommandtimeout=30000;connectiontimeout=30000
4.  installedPlugins.json 插件文件配置
5.  插件源代码位于Plugins 生成路径为Urs.Shop/Plugins
6.  后台管理在 Urs.Shop/Areas/Admin   后台账号urshop 密码 admin
7.  后台富文本编辑器采用百度编辑器 UEditorNetCore,配置文件为 config.json
8.  微信SDK之前打包了 WeixinSDK http://www.weixinsdk.net
9.  miniprogram 为小程序代码
10. 以上为基础配置，只需要把备份数据库还原，并配置好dataSettings.json链接即可使用；
11. 在线教程文档正在编辑中，欢迎持续关注
12. 功能还在完善，如有bug或配置问题欢迎留言或私信; 
13. 仓库地址 https://gitee.com/urselect/urshop 

#### <h2>参与贡献</h2>

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



 **<h2>🙏🙏🙏 点个Star</h2>** 
 **如果您觉得这个项目还不错, 可以右上角帮我点个star, 支持一下作者 ☜(ﾟヮﾟ☜)** 


